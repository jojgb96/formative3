package formative3.template;

public class Main {

	public static void main(String[] args) {
		Standard stand = new Standard();
		stand.setDurasi(5);// jam
		Premium stand2 = new Premium();
		stand2.setDurasi(5);// jam

		System.out.println("Warnet Standard : Rp" + stand.getBiaya() + "/jam");
		System.out.println("Durasi : " + stand.getDurasi());
		System.out.println("Total : Rp" + stand.getHarga());

		System.out.println("Warnet Premium : Rp" + stand2.getBiaya() + "/jam");
		System.out.println("Durasi : " + stand2.getDurasi());
		System.out.println("Total : Rp" + stand2.getHarga());

		/*Disini yang template method adalah getHarga,getHarga merupakan abstrak method dari kelas 
		 * warnet ,method tsb diwariskan ke	class Standard dan Premium yang override sehingga
		 * nama method sama hanya fungsi pada setiap kelas beda beda
		 * */
	}

}
