package formative3.template;

public abstract class Warnet {
	protected int durasi;

	public int getDurasi() {
		return durasi;
	}

	public void setDurasi(int durasi) {
		this.durasi = durasi;
	}
	
	public abstract int getHarga();
}
