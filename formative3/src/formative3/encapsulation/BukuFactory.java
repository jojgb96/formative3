package formative3.encapsulation;

public class BukuFactory {
	public static Buku newInstance(String tipe) {
		if (tipe.equals("kamus")) {
			return new Kamus();
		} else if(tipe.equals("majalah")) {
			return new Majalah();
		}else if(tipe.equals("tabloid")) {
			return new Tabloid();
		}
		
		return null;
	}
}
