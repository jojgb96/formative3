package formative3.encapsulation;

public class Buku {
	private String judul;
	private int halaman;
	
	
	public String getJudul() {
		return judul;
	}
	public int getHalaman() {
		return halaman;
	}
	public void setJudul(String judul) {
		this.judul = judul;
	}
	public void setHalaman(int halaman) {
		this.halaman = halaman;
	}
	
	
}
