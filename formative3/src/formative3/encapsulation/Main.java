package formative3.encapsulation;

public class Main {

	public static void main(String[] args) {
		Buku b1 =  new BukuFactory().newInstance("kamus");//membuat objek dari kelas buku dengan instance dari class kamus yang merupakan child dari class buku
		Buku b2 =  new BukuFactory().newInstance("majalah");//membuat objek dari kelas buku dengan instance dari class majalah  yang merupakan child dari class buku
		Buku b3 = new BukuFactory().newInstance("tabloid");//membuat objek dari kelas buku dengan instance dari class tabloid yang merupakan child dari class buku
		
		b1.setJudul("Bahasa Indonesia");
		b1.setHalaman(100);
		
		b2.setJudul("bobo");
		b2.setHalaman(20);
		
		b3.setJudul("trubus");
		b3.setHalaman(30);

		System.out.println(b1 instanceof Kamus);//true karena b1 instance dari kamus
		System.out.println(b2 instanceof Kamus);//false karena b1 instance dari kamus
		System.out.println(b3 instanceof Kamus);//false karena b1 instance dari kamus
	}

}
