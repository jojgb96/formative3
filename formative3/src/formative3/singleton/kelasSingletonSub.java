package formative3.singleton;

public class kelasSingletonSub {
	static kelasSingletonSub obj1 = new kelasSingletonSub();
	public String nama;
	private kelasSingletonSub() {
		System.out.println("Object Telah Dibuat");
	}
	
	private kelasSingletonSub(String nama) {
		this.nama = nama;
	}
	
	public static kelasSingletonSub getInstance() {
		return obj1;
	}
}
