package formative3.singleton;

public class SingletonMain {

	public static void main(String[] args) {
		/// saat kita membuat singleton berarti kita akan menciptakan 1 instance saja
		// object ini akan sering kita pakai di berbagai tempat(seperti config pada aplikasi)
		kelasSingletonSub obj1 = kelasSingletonSub.getInstance();
		kelasSingletonSub obj2 = kelasSingletonSub.getInstance();
		kelasSingletonSub obj3 = kelasSingletonSub.getInstance();
		
		
		//// output hanya diprint satu kali 
		
		System.out.println(obj1 == obj2);// akan mencetak true karena merefer pada objek yang sama
		System.out.println(obj2 == obj3);
	}

}
