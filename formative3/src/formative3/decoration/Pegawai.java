package formative3.decoration;

public class Pegawai extends ManusiaDecorator {

	public Pegawai(Manusia manusiaDecorator) {
		super(manusiaDecorator);
		
	}

	@Override
	public void bernapas() {
		manusiaDecorator.bernapas();
		berlari(manusiaDecorator);
	}

	public void berlari(Manusia manusiaDecorator) {
		System.out.println("bisa lari");
	}
}
