package formative3.decoration;

public class ManusiaDecorator implements Manusia {
	protected Manusia manusiaDecorator;
	
	public ManusiaDecorator(Manusia manusiaDecorator) {
		this.manusiaDecorator = manusiaDecorator;
	}
	

	@Override
	public void bernapas() {
		 manusiaDecorator.bernapas();
	}
	
}
